<?php

class Custom_Shortcodes{


	public function __construct(){
		$this->testidata = array();
	}


	public function slider(){


	    $about_preview_query = new WP_Query('post_type=my_testimonial');


	    while ( $about_preview_query->have_posts() ) {
	        $about_preview_query->the_post();
	        echo "<h1>";
	        the_title();
	        echo "</h1>";
	    }
			
	}

	public function featured_testi(){

	$testidata = new WP_Query(array('post_type' => 'my_testimonial', 'posts_perpage' => 1, 'orderby' => 'rand' ));

	$testidata->the_post();

	$title = get_the_title();
	$name = get_post_meta(get_the_id(),"name",true);
	$city = get_post_meta(get_the_id(),"city",true);
	$image = get_the_post_thumbnail( get_the_id(), 'thumbnail');

		echo '		<span class="custom-exp-title">'.get_the_title().'</span>
					<span class="custom-exp-desc">'.get_the_content().'</span>
					<span class="custom-exp-profile">'.$image.'</span>
					<span class="name">'.$name.', <span class="color-00abee">'.$city.'</sapn></span>';
	}

	public function slider_testi(){



		$testidata = new WP_Query('post_type=my_testimonial');

		echo '  <div class="custom-slider-dot">
					<div style="float:right" id="slideNav"></div>
				</div>';

		echo "<div id='custom-slider'>";
		while ( $testidata->have_posts() ) {
	 		$testidata->the_post();
	
	 		$title = get_the_title();
			$name = get_post_meta(get_the_id(),"name",true);
			$city = get_post_meta(get_the_id(),"city",true);
			$image = get_the_post_thumbnail( get_the_id(), 'thumbnail');

		echo '
		  	<div class="slides">
					<div class="custom-slider-image-container">
						 <div class="slider-image">'.$image.'</div>
						 <span class="name">'.$name.', <span class="color-00abee">'.$city.'</sapn></span>
					</div>
					<div class="slider-content-container">
						<h3>'.get_the_title().'</h3>
						<p>
							 '.get_the_content().'
						</p>
					</div>
				</div>';
		}
		echo "</div>";

	}

}