<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//initialize testimonials
include_once('testimonial.php');
$testimonial = new Custom_Posts();


//initialize shortcode
include_once('shortcodes.php');
$custom_shortcode = new Custom_Shortcodes();

add_shortcode('slider1', array($custom_shortcode, 'slider_testi'));
add_shortcode('featured_testi', array($custom_shortcode, 'featured_testi'));
//add_shortcode('my-code2', array($custom_shortcode, 'dynamicshortcode'));

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Genesis Sample Theme' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.1.2' );

//* Enqueue Google Fonts
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );
function genesis_sample_google_fonts() {
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Montserrat', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Droid+Serif', array(), CHILD_THEME_VERSION );
}

//enque style and scripts here funtion
function theme_name_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_script( 'jquery-1.12.0', 'http://localhost/invisaligndealottawa/wp-content/themes/invisalign/js/jquery.js');
	wp_enqueue_script( 'jquery-cycle', 'http://localhost/invisaligndealottawa/wp-content/themes/invisalign/js/jquery.cycle.js');
	wp_enqueue_script( 'slider-js', 'http://localhost/invisaligndealottawa/wp-content/themes/invisalign/js/core.js');
}

//call enque function
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );


function mp_cta_genesis(){
	?>

	<div class="home-socials-container">
		<div class="wrap">
			<div style="float:left" class="home-social-text">
				Highly Prominent Keywords
			</div>
			<div style="float:right">
					<div class="fb-like" data-href="" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
					<div class="g-plusone" data-size="medium"></div>
			</div>
		</div>
	</div>

	<?php
}

add_action( 'genesis_before_header', 'mp_cta_genesis' );


register_sidebar(array(
		'name' => 'Homepage banner',
		'id'   => 'custom-homepage-banner',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-homepage-banner-id" class="custom-banner-id-class">',
		'after_widget'  => '</div>'
	));
register_sidebar(array(
		'name' => 'Invisalign Patient',
		'id'   => 'custom-invisalign-main-id',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-invisalign-id" class="custom-invisalign-class">',
		'after_widget'  => '</div>'
	));
register_sidebar(array(
		'name' => 'Schedule Form',
		'id'   => 'custom-form-main-id',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-form-id" class="custom-form-class">',
		'after_widget'  => '</div>'
	));

register_sidebar(array(
		'name' => 'Experience Widget',
		'id'   => 'custom-exp-main-id',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-exp-id" class="custom-exp-class">',
		'after_widget'  => '</div>'
	));
register_sidebar(array(
		'name' => 'Living  With Invisalign',
		'id'   => 'custom-living-main-id',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-living-id" class="custom-living-class">',
		'after_widget'  => '</div>'
	));
register_sidebar(array(
		'name' => 'Callback',
		'id'   => 'custom-callback-main-id',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-callback-id" class="custom-callback-class">',
		'after_widget'  => '</div>'
	));
register_sidebar(array(
		'name' => 'Custom Slider',
		'id'   => 'custom-slider-main-id',
		'description'   => 'This is a menu area.',
		'before_widget' => '<div id="custom-slider-id" class="custom-slider-class">',
		'after_widget'  => '</div>'
	));



   register_sidebar(array(
			'name' => 'Logo Holder',
			'id'   => 'custom-logo-main-id',
			'description'   => 'This is a menu area.',
			'before_widget' => '<div id="custom-logo-id" class="custom-logo-class">',
			'after_widget'  => '</div>'
		));
	register_sidebar(array(
			'name' => 'Subscribe Widget',
			'id'   => 'custom-subscribe-main-id',
			'description'   => 'This is a menu area.',
			'before_widget' => '<div id="custom-subscribe-id" class="custom-subscribe-class">',
			'after_widget'  => '</div>'
		));


//Position Widget Header
function subscribeFooter()  {

	?>
<div class="wrap">
 	<?php dynamic_sidebar('Callback'); ?>
 	<?php dynamic_sidebar('Custom Slider'); ?>
 </div>
 <div class="custom-logo-inner">
 	<div class="wrap">
 		<?php dynamic_sidebar('Logo Holder') ?>
 	</div>
</div> 
	<?php	

	echo "<div class='custom-subscribe-inner'>";
		echo "<div class='wrap'>";
				dynamic_sidebar('Subscribe Widget');
		echo "</div>";
	echo "</div>";
}






// filter the Gravity Forms button type
add_filter( 'gform_submit_button_1', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='custom-form-button' id='gform_submit_button_{$form['id']}'><span class='btn-form-text'>Click here to Schedule Your 
FREE Consultation Now</span></button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_2', 'form_submit_button2', 10, 2 );
function form_submit_button2( $button, $form ) {
    return "<button class='custom-form-button custom-subscribe-button' id='gform_submit_button_{$form['id']}'><span class='btn-form-text'>SUBSCRIBE</span></button>";
}


remove_action( 'genesis_footer', 'genesis_do_footer' );
//.add_action("genesis_footer",'copyright');

add_action( 'genesis_footer', 'sp_custom_footer' );
add_action ('genesis_before_footer','subscribeFooter');

function sp_custom_footer() {
	?>
		<div class="wrap">
			<div class="one-half custom-footer-menu">
			 	<?php wp_nav_menu(); ?>
			 	<article>
			 		<div class="footer-logo"></div>
			 	</article>
			</div>
			<div class="one-half custom-footer-contact">
				<article>
					<div class="custom-header-container">
						<span class="custom-header-text1">FREE Consultation (24/7)</span>
						<span class="custom-header-text2">613 - 763 - 2392</span>
						<span class="custom-header-text3">info@invisalign.com</span>
						<span class="custom-header-text4">Serving the City of Ottawa</span>

						<span class="custom-header-text5" style="margin-top:29px">525 Legget Drive</span>
						<span class="custom-header-text5">Suite 120</span>
						<span class="custom-header-text5">Kanata, ON K2K 2W2</span>
					</div>
				</article>
			</div>
		</div>
	<?php
}


function copyright() {
	echo "<div class='after-footer-main'>";
	echo "<div class='wrap'>";
    echo "<div class='after-footer'>Copyright © 2016 Invisalign All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terms & Conditions&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Contact Us</div>";
    echo "</<div>";
    echo "</div>";
}
add_action( 'wp_footer', 'copyright' );

function facebookLike(){
	?>
	    <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<script src="https://apis.google.com/js/platform.js" async defer></script>
	<?php
}

add_action('genesis_after_header','facebookLike');
