<?php


class Custom_Posts {


 public function __construct(){

		add_theme_support('post-thumbnails');
		add_post_type_support( 'my_testimonial', 'thumbnail' );  
	
 
        add_action( 'init', array(&$this, 'create_post_testimonial') );
      
	}

  
	function create_post_testimonial() {
	        register_post_type( 'my_testimonial',
	            array(
	                'labels' => array(
	                    'name' => __( 'Testimonials' ),
	                    'singular_name' => __( 'Testimonial' )
	                ),
	                'public' => true,
	                'has_archive' => true,
	                 'supports'  => array( 'title', 'editor', 'author','excerpt','custom-fields','comments')
	            )
	        );
	}

   
}